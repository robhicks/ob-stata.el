# This project is no longer maintained

According to #10, `ob-stata.el` is broken with an updated version of Emacs-Speaks-Statistics.  I am not going to fix this. 

I am now using [`stata_kernel`](https://github.com/kylebarron/stata_kernel) to execute stata code and organize output in `emacs` as described [here](https://rlhick.people.wm.edu/posts/stata_kernel_emacs.html).

# ob-stata.el
Modified version of `ob-stata.el` from the EMACS repository for providing `stata` support in EMACS org-mode.  Modifications add these features:
1. strips most commands in org-mode src output (see remaining issues below)
2. refines the regex replace to avoid deleting output from some stata commands
3. supports line continuation (`///`) for stata commands

A more detailed description of what this does and how to use it is [found here](http://rlhick.people.wm.edu/posts/stata-and-literate-programming-in-emacs-org-mode.html).

The modifications described above only work with src blocks with `:results output` and for stata sessions invoked by `:session`.  Custom session names are now supported. (e.g. `:session stata-session` will execute).

## Remaining issues
While this code does a pretty good job of eliminating commands in results output, there are some instances with line continuation and long commands where commands may not be stripped from output.
